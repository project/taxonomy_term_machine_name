<?php

namespace Drupal\taxonomy_term_machine_name\Utils;

/**
 * Implements Utility class.
 */
class Utility {

  /**
   * Field type of provide by this module.
   */
  const MODULE_NAME = 'taxonomy_term_machine_name';

  /**
   * Field type of provide by this module.
   */
  const FIELD_TYPE = self::MODULE_NAME . '_default';

  /**
   * Field name of provide by this module.
   */
  const FIELD_NAME = 'machine_name';

}
