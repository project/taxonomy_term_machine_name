<?php

namespace Drupal\taxonomy_term_machine_name\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'taxonomy_term_machine_name' field type.
 *
 * @FieldType(
 *   id = "taxonomy_term_machine_name_default",
 *   label = @Translation("Machine Name"),
 *   default_formatter = "taxonomy_term_machine_name_default",
 *   default_widget = "taxonomy_term_machine_name_default",
 *   cardinality = 1,
 *   locked = TRUE,
 * )
 */
class TaxonomyTermMachineNameDefaultItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
          'default' => '',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(\t('Machine Name'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $machine_name = $this->getValue();
    return (empty($machine_name));
  }

}
