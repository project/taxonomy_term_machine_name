<?php

namespace Drupal\taxonomy_term_machine_name\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy_term_machine_name\Utils\Utility;

/**
 * Plugin implementation of the 'taxonomy_term_machine_name' widget.
 *
 * @FieldWidget(
 *   id = "taxonomy_term_machine_name_default",
 *   label = @Translation("Machine Name"),
 *   field_types = {
 *     "taxonomy_term_machine_name_default",
 *   },
 * )
 */
class TaxonomyTermMachineNameDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    return [
      'value' => [
        '#type' => 'machine_name',
        '#default_value' => $items[$delta]->getString(),
        '#element_validate' => [[static::class, 'validateUniqueMachineName']],
        '#cache' => ['max-age' => 0],
      ],
    ];
  }

  /**
   * Validate the machine name is unique.
   */
  public static function validateUniqueMachineName($element, FormStateInterface $form_state, $form) {
    if (!empty($value = $form_state->getValue(Utility::FIELD_NAME, [0 => ['value' => '']])) && $database = \Drupal::database()) {
      $vid = $form_state->getValue('vid') ?: '*';
      $tid = $form_state->getValue('tid') ?: '0';
      $query = $database->select('taxonomy_term__' . Utility::FIELD_NAME, 'ttm');
      $query->addField('ttm', 'entity_id');
      $query->condition('ttm.bundle', $vid, '=');
      $query->condition('ttm.entity_id', $tid, '!=');
      $query->condition('ttm.' . Utility::FIELD_NAME . '_value', $value[0]['value'], '=');
      $ret = $query->execute()->fetch();
      if (isset($ret->entity_id)) {
        $form_state->setError($element, \t('The machine-readable name is already in use. It must be unique.'));
      }
    }
  }

}
