<?php

namespace Drupal\taxonomy_term_machine_name\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\ConfirmFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy_term_machine_name\Utils\Utility;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implementing system configuration forms.
 */
class TaxonomyTermMachineNameForm extends EntityConfirmFormBase implements ConfirmFormInterface {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return Utility::MODULE_NAME . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getBaseFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the form array using the possibly updated entity in form state.
    $form = $this->form($form, $form_state);

    // Retrieve and add the form actions array.
    $actions = $this->actionsElement($form, $form_state);
    if (!empty($actions)) {
      $form['actions'] = $actions;
    }
    $this->addDependencyListsToForm($form);
    return $form;
  }

  /**
   * Adds form elements to list affected configuration entities.
   *
   * @param array $form
   *   The form array to add elements to.
   *
   * @see \Drupal\Core\Config\ConfigManagerInterface::getConfigEntitiesToChangeOnDependencyRemoval()
   */
  protected function addDependencyListsToForm(array &$form) {
    // Get the dependent entities.
    $dependent_entities = \Drupal::service('config.manager')->getConfigEntitiesToChangeOnDependencyRemoval('module', [Utility::MODULE_NAME]);

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_types = [];

    // List up of updates entities.
    $entity_updates = [];
    foreach ($dependent_entities['update'] as $entity) {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
      $entity_type_id = $entity->getEntityTypeId();
      if (!isset($entity_updates[$entity_type_id])) {
        $entity_type = $entity_type_manager->getDefinition($entity_type_id);
        // Store the ID and label to sort the entity types and entities later.
        $label = $entity_type->getLabel();
        $entity_types[$entity_type_id] = $label;
        $entity_updates[$entity_type_id] = [
          '#theme' => 'item_list',
          '#title' => $label,
          '#items' => [],
        ];
      }
      $entity_updates[$entity_type_id]['#items'][$entity->id()] = $entity->getOriginalId();
    }
    if (!empty($dependent_entities['update'])) {
      // Add a weight key to the entity type sections.
      \asort($entity_types, SORT_FLAG_CASE);
      $weight = 0;
      foreach ($entity_types as $entity_type_id => $label) {
        $entity_updates[$entity_type_id]['#weight'] = $weight;
        // Sort the list of entity labels alphabetically.
        \ksort($entity_updates[$entity_type_id]['#items'], SORT_FLAG_CASE);
        $weight++;
      }
    }
    if (!empty($entity_updates)) {
      $form['entity_updates'] = \array_merge([
        '#type' => 'details',
        '#title' => $this->t('Configuration updates'),
        '#description' => $this->t('The listed configuration will be updated.'),
        '#open' => TRUE,
        '#access' => TRUE,
      ], $entity_updates);
    }

    // List up of deletes entities.
    $entity_deletes = [];
    foreach ($dependent_entities['delete'] as $entity) {
      $entity_type_id = $entity->getEntityTypeId();
      if (!isset($entity_deletes[$entity_type_id])) {
        $entity_type = $entity_type_manager->getDefinition($entity_type_id);
        // Store the ID and label to sort the entity types and entities later.
        $label = $entity_type->getLabel();
        $entity_types[$entity_type_id] = $label;
        $entity_deletes[$entity_type_id] = [
          '#theme' => 'item_list',
          '#title' => $label,
          '#items' => [],
        ];
      }
      $entity_deletes[$entity_type_id]['#items'][$entity->id()] = $entity->getOriginalId();
    }
    $delete_weight = 0;
    if (!empty($dependent_entities['delete'])) {
      // Add a weight key to the entity type sections.
      \asort($entity_types, SORT_FLAG_CASE);
      foreach ($entity_types as $entity_type_id => $label) {
        if (isset($entity_deletes[$entity_type_id])) {
          $entity_deletes[$entity_type_id]['#weight'] = $delete_weight;
          // Sort the list of entity labels alphabetically.
          \ksort($entity_deletes[$entity_type_id]['#items'], SORT_FLAG_CASE);
          $delete_weight++;
        }
      }
    }
    if (!empty($entity_deletes)) {
      $form['entity_deletes'] = \array_merge([
        '#type' => 'details',
        '#title' => $this->t('Configuration deletions'),
        '#description' => $this->t('The listed configuration will be deleted.'),
        '#open' => TRUE,
        '#access' => TRUE,
      ], $entity_deletes);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    /** @var \Drupal\Core\Config\ConfigManager */
    $config_manager = \Drupal::service('config.manager');

    // Remove configurations of all dependencies.
    $config_manager->uninstall('module', Utility::MODULE_NAME);

    /*+
     * "Fields pending deletion" stopping module unistall.
     *
     * @see https://drupal.stackexchange.com/questions/244275/fields-pending-deletion-stopping-module-unistall-how-to-delete-manually
     */
    field_purge_batch(10000);

    // Redirect to modules uninstall page.
    \Drupal::messenger()->addStatus($this->t('Removed all storages of field the "Machine Name".'));
    $redirect_response = new RedirectResponse($this->getCancelUrl()->toString());
    $redirect_response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want delete of machine name field?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromUserInput('/admin/modules/uninstall');
  }

}
